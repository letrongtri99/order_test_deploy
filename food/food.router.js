const express = require("express");
const foodModel = require("./food.model");
const foodRouter = express.Router();
const auth = require("../auth.js");
const app = express();

foodRouter.post(`/create`, auth, (req, res) => {
  foodModel
    .create({
      name: "Xôi gà",
      category: "MONGA",
      ingredients: [
        {
          "6016bbf3e01ffc39a44ffbe9": 1,
          "601639a611aec9121035d814": 1,
        },
      ],
      imageUrl:
        "https://images.foody.vn/res/g104/1030916/s120x120/a9e720b1-eab5-40de-80cc-8f395457-f5f8e30f-201003184507.jpeg",
    })
    .then((data) => {
      return res.json({
        data: data,
      });
    })
    .catch((e) => {
      console.log(e);
    });
});

foodRouter.get(`/getAllData`, auth, (req, res) => {
  foodModel
    .find({})
    .then((data) => {
      return res.json({
        data: data,
      });
    })
    .catch((e) => {
      console.log(e);
    });
});

foodRouter.get(`/getById`, auth, (req, res) => {
  foodModel
    .findOne({
      _id: req.query.id,
    })
    .then((data) => {
      return res.json({
        data: data,
      });
    })
    .catch((e) => {
      console.log(e);
    });
});

module.exports = foodRouter;
