const express = require("express");
const userModel = require("./user.model");
const userRouter = express.Router();
const bcryptjs = require("bcryptjs");
const auth = require("../auth.js");
const secret = require("../secret.js");
const jwt = require("jsonwebtoken");
const app = express();

userRouter.post(`/create`, auth, (req, res) => {
  const hashPassword = bcryptjs.hashSync("123456a@", 10);
  userModel
    .create({
      email: "trilt",
      password: hashPassword,
    })
    .then((data) => {
      return res.json({
        data: data,
      });
    })
    .catch((e) => {
      console.log(e);
    });
});

userRouter.post(`/login`, (req, res) => {
  userModel
    .findOne({ email: req.body.email })
    .then((data1) => {
      if (!data1) {
        res.json({
          success: false,
          message: "Email không tồn tại",
        });
      } else if (!bcryptjs.compareSync(req.body.password, data1.password)) {
        res.json({
          success: false,
          message: "Sai mật khẩu",
        });
      } else {
        const token = jwt.sign({ email: data1.email }, secret.author, {
          expiresIn: 60 * 60 * 24,
        });
        res.json({
          success: true,
          message: "Đăng nhập thành công",
          token: `Bearer ${token}`,
        });
      }
    })
    .catch((e) => {
      console.log(e);
    });
});

userRouter.get(`/checkSession`, (req, res) => {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  if (token == null) {
    return res.json({
      success: false,
      message: "Unauthorized",
    });
  } else {
    jwt.verify(token, secret.author, (err, user) => {
      if (err) {
        return res.json({
          success: false,
          message: "Unauthorized",
        });
      }
      return res.json({
        success: true,
        data: user,
      });
    });
  }
});

userRouter.get(`/logout`, auth, (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      return res.json({
        success: false,
        message: "Đăng xuất không thành công",
      });
    } else {
      return res.json({
        success: true,
        message: "Đăng xuất thành công",
      });
    }
  });
});

module.exports = userRouter;
