const secret = require("./secret.js");
const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  if (token == null) {
    return res.json({
      success: false,
      message: "Unauthorized",
    });
  } else {
    jwt.verify(token, secret.author, (err, user) => {
      if (err) {
        return res.json({
          success: false,
          message: "Unauthorized",
        });
      }
      return next();
    });
  }
};
